var fs = require("fs");
var express = require('express');
var app = express();

var filename = "file.txt";

app.get('/', function (req, res) {
	res.send("Usage: <br/> /stream for reading file content with streams <br/> " +
			 "/bufferAsync for reading file content asynchronously <br/> " + 
			 "/bufferSync for reading file content synchronously");
});

app.get('/stream', function (req, res) {
	//pipe the readable stream to res(ult)
  	fs.createReadStream(filename).pipe(res);
});

app.get('/bufferAsync', function (req, res) {
  	//asynchronous method
  	fs.readFile(filename, "utf8", function (err, data) {
  		if (!err) {
  			res.send(data);
  		} else {
  			console.log(err);
  		}
  	});
});

app.get('/bufferSync', function (req, res) {
	// synchronous method, read into buffer and then send the buffer
	var buf = fs.readFileSync(filename, "utf8"); 
  	res.send(buf);
});

var server = app.listen(8080);