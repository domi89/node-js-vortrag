var fs = require("fs");
var express = require('express');
var app = express();

var filename = "file.txt";


app.get('/bufferAsync', function (req, res) {
  	//asynchronous method
  	fs.readFile(filename, "utf8", function (err, data) {
  		if (!err) {
  			res.send(data);
  		} else {
  			console.log(err);
  		}
  	});
});


var server = app.listen(8081);