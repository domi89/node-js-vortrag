var fs = require("fs");
var express = require('express');
var app = express();

var filename = "file.txt";

app.get('/stream', function (req, res) {
	//pipe the readable stream to res(ult)
  	fs.createReadStream(filename).pipe(res);
});


var server = app.listen(8080);