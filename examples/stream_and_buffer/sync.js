var fs = require("fs");
var express = require('express');
var app = express();

var filename = "file.txt";

app.get('/bufferSync', function (req, res) {
	// synchronous method, read into buffer and then send the buffer
	var buf = fs.readFileSync(filename, "utf8"); 
  	res.send(buf);
});

var server = app.listen(8082);