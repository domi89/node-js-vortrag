var express = require('express');
var app = express();
var logic = require('asdf')


app.get('/', function (req, res) {
	res.send("If you can read this, everything is fine!");
});


var server = app.listen(8080);

//do some work after the server has been started
fibonacci(45);
console.log("Done");


function fibonacci(n) {
  if (n < 2)
    return 1;
  else
    return fibonacci(n-2) + fibonacci(n-1);
}